export interface Weather {
    city:string;
    description:string;
    mainDescription:string;
    temp:number;
    windSpeed:number;
    pressure:number;
    humidity:number;
  }