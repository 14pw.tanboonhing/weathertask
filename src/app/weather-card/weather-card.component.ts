import { Component, OnInit, Input } from '@angular/core';
import { Weather } from '../weather';
import {WeatherService} from '../weather.service';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.css']
})
export class WeatherCardComponent implements OnInit {
  @Input() cardId:number;
  weather: Weather;
  city = '';
  show=false;
  loading=false;
  submitted=false;
  error='';
  imgUrl='';
  iconUrl='';
  imgAlt='';
  interval:any;
  openInput():void{
    this.show=true;
  }
  setImageUrl(id:number , icon:string):void{
    this.iconUrl = `http://openweathermap.org/img/wn/${icon}@2x.png`;
    if(id>=200 && id <=299)
    {
      //Thunderstorm
      this.imgAlt = "Thunderstorm";
      this.imgUrl = "https://bjournal.co/wp-content/uploads/2019/06/thunderstorm-700x325.jpg";
    }
    else if(id >=300 && id <=399)
    {
      //Drizzle
      this.imgAlt = "Drizzle";
      this.imgUrl = "https://bloximages.chicago2.vip.townnews.com/madison.com/content/tncms/assets/v3/editorial/6/ec/6ec35d32-1a73-5cd6-ade5-4bc62be4f3ac/59de0f0796db6.image.png?crop=640%2C463%2C277%2C178";
    }
    else if(id >=500 && id <=599)
    {
      //Rain
      this.imgAlt = "Rain";
      this.imgUrl = "https://www.harwichandmanningtreestandard.co.uk/resources/images/10469131.jpg?display=1&htype=0&type=responsive-gallery";
    }
    else if(id >=600 && id <=699)
    {
      //Snow
      this.imgAlt = "Snow";
      this.imgUrl = "https://i.pinimg.com/originals/05/73/61/057361746a4d4495f580bc871e74e9c5.jpg";
    }
    else if(id >=700 && id <=799)
    {
      //Atmosphere
      this.imgAlt = "Atmosphere";
      this.imgUrl = "https://i1.pickpik.com/photos/595/755/579/mist-fog-cloud-smoke-preview.jpg";
    }
    else if(id ==800)
    {
      //Clear
      this.imgAlt = "Clear";
      this.imgUrl = "https://4.bp.blogspot.com/_tOJ-lqBX0wQ/TKzHK2u3PmI/AAAAAAAAAD0/yBauow-LYT4/s1600/Kayla+October+4+012.jpg";
    }
    else if(id >=801 && id <=899)
    {
      //Clouds
      this.imgAlt = "Clouds";
      this.imgUrl = "https://www.metoffice.gov.uk/binaries/content/gallery/metofficegovuk/hero-images/weather/cloud/altocumulus.jpg";
    }
  }
  changeCity():void{
    this.show=true;
    this.submitted = false;
    this.error = '';
    this.weather = undefined;
    this.imgUrl='';
    localStorage.removeItem("weather" + this.cardId);
  }
  checkApi():void{
    if(this.submitted ==true)
    {
      return;
    }
    this.submitted = true;
    this.loading = true;
      this.weatherService.getWeather(this.city).subscribe(
        (data)=>{
          this.loading = false;
          this.setImageUrl(data['weather'][0].id ,data['weather'][0].icon);
          if(data != null)
          {
            this.weather = <Weather>{
              city:data['name'],
              description:data['weather'][0].description,
              mainDescription:data['weather'][0].main,
              temp:data['main'].temp,
              windSpeed:data['wind'].speed,
              pressure:data['main'].pressure,
              humidity:data['main'].humidity,
            };
            this.setStorageData();
          }
          },
        (err) => {
          this.loading = false;
          this.submitted = false;
          console.log(err);
          this.error = err;
        }
        );
  }
  refreshData(): void{
    if(this.weather == undefined)
    {
      return;
    }
    this.weatherService.getWeather(this.weather.city).subscribe(
      (data)=>{
        this.setImageUrl(data['weather'][0].id ,data['weather'][0].icon);
        if(data != null)
        {
          this.weather = <Weather>{
            city:data['name'],
            description:data['weather'][0].description,
            mainDescription:data['weather'][0].main,
            temp:data['main'].temp,
            windSpeed:data['wind'].speed,
            pressure:data['main'].pressure,
            humidity:data['main'].humidity,
          };
          if (localStorage.getItem("weather" + this.cardId)) {
            this.setStorageData();
            }
        }
        });
  }
  getStorageData(): void {
    if (localStorage.getItem("weather" + this.cardId)) {
      this.show=true;
      this.submitted = true;
      let storeData = JSON.parse(localStorage.getItem("weather" + this.cardId));
      this.weather = <Weather>storeData[0];
      this.imgUrl = storeData[1];
      this.iconUrl = storeData[2];
      this.imgAlt = storeData[3];
    }
  }
  setStorageData(): void {
    localStorage.setItem("weather" + this.cardId, JSON.stringify([this.weather ,this.imgUrl,
    this.iconUrl,
    this.imgAlt ]));
  }
  constructor(private weatherService:WeatherService) { }

  ngOnInit(): void {
    this.getStorageData();
    this.interval = setInterval(() => { 
      this.refreshData(); 
  }, 15000);
  }

}
