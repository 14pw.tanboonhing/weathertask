import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private weatherUrl = 'http://api.openweathermap.org/data/2.5/weather?q=London&appid=8f9400732b04fc76984eb5fc3ddaf65a';  // URL to web api
  private apiKey ='8f9400732b04fc76984eb5fc3ddaf65a'
  getWeather(loc:string) {
    return this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${loc}&appid=${this.apiKey}`)
    .pipe(
      catchError(this.handleError)
    );
  }
  private handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      error.error.message);
  };
  constructor(private http: HttpClient) { }
}
